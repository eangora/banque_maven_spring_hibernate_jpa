package org.gestion.bp.controller;

import java.util.List;

import javax.validation.Valid;

import org.gestion.bp.entities.Compte;
import org.gestion.bp.entities.Operation;
import org.gestion.bp.metier.IBanqueMetier;
import org.gestion.bp.models.BanqueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BanqueController {
	
	@Autowired
	private IBanqueMetier metier;
	//banque est le fichier jsp
	//lien http://localhost:8080/bp/index
	@RequestMapping(value="/index")
	public String index(Model model) {
		//Le model banqueForm sera utilis� dans la form de banque.jsp
		model.addAttribute("banqueForm", new BanqueForm());
		return "banque";
	}
	
	//Les donn�es seront charg�es dans BanqueForm
	//valid permet � spring de valider le formulaire et bindingResult
	@RequestMapping(value="/chargerCompte")
	public String charger(@Valid BanqueForm bf,
			BindingResult bindingResult,Model model) {
		//Si il y a une erreur dans le formulaire on revient sur le formulaire on interroge pas la couche metier
		if(bindingResult.hasErrors()) {
			return "banque";
		}
		//Appel de la methode chargerCompte
		chargerCompte(bf);
		model.addAttribute("banqueForm", bf);
		return "banque";
	}
	//On effectue l'op�ration
	@RequestMapping(value="/saveOperation")
	public String saveOp(@Valid BanqueForm bf,BindingResult bindingResult) {
		
		try {
			//Si il y a une erreur dans le formulaire on revient sur le formulaire on interroge pas la couche metier
			if(bindingResult.hasErrors()) {
				return "banque";
			}
			
			if(bf.getAction()!=null) {
			//on teste le type d'op�ration
			if (bf.getTypeOperation().equals("VER")) {
				metier.versement(bf.getCode(), bf.getMontant(), 1L);
				
			}else if(bf.getTypeOperation().equals("RET")) {
				metier.retrait(bf.getCode(), bf.getMontant(), 1L);
				
			}else if(bf.getTypeOperation().equals("VIR")){
				metier.virement(bf.getCode(), bf.getCode2(), bf.getMontant(), 1L);
				
			}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			bf.setException(e.getMessage());;
		}
		//Appel de la m�thode charger
		chargerCompte(bf);
		
		return "banque";
	}
	
	public void chargerCompte(BanqueForm bf) {
		try {
			//On charge le compte et charger le code � partir de la couche metier
			Compte c = metier.consulterCompte(bf.getCode());
			//Stockage dans le model
			bf.setTypeCompte(c.getClass().getSimpleName());
			bf.setCompte(c);
			//Pagination
			int posi = bf.getNbLignes()*bf.getPage();
			//On charge les op�tations
			List<Operation> ops = metier.consulterOperations(bf.getCode(),posi,bf.getNbLignes());
			//Stockage de la liste
			bf.setOperations(ops);
			//nombre de pages
			long nbOperation = metier.getNombreOperation(bf.getCode());
			bf.setNbpages((int) (nbOperation/bf.getNbLignes())+1);//Les virgules sont ignor�es
		} catch (Exception e) {
			bf.setException(e.getMessage());
		}
		
	}


}
