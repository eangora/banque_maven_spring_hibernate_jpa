package org.gestion.bp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.gestion.bp.entities.Client;
import org.gestion.bp.entities.Compte;
import org.gestion.bp.entities.Employe;
import org.gestion.bp.entities.Groupe;
import org.gestion.bp.entities.Operation;

public class BanqueDaoImpl implements IBanqueDao {
	//Pour gerer la persistence on va faire appel � JPA
	@PersistenceContext
	private EntityManager em;

	@Override
	public Client addClient(Client c) {
		em.persist(c);
		return c;
	}

	@Override
	public Employe addEmploye(Employe e, Long codeSup) {
		if(codeSup!=null) {
			Employe sup=em.find(Employe.class, codeSup);
			e.setEmployeSup(sup);
		}
		em.persist(e);
		return e;
	}

	@Override
	public Groupe addGroupe(Groupe g) {
		em.persist(g);
		return g;
	}

	@Override
	public void addEmployeToGroupe(Long codeEmp, Long codeGroupe) {
		Employe e = em.find(Employe.class, codeEmp);
		Groupe g = em.find(Groupe.class, codeGroupe);
		e.getGroupes().add(g);
		g.getEmployes().add(e);
	}

	@Override
	public Compte addCompte(Compte c, Long codeCli, Long codeEmp) {
		//On charge les donn�es
		Client cli = em.find(Client.class, codeCli);
		Employe emp = em.find(Employe.class, codeEmp);
		//On va les associer au compte
		c.setClient(cli);
		c.setEmploye(emp);
		//Enregistrer le compte
		em.persist(c);
		return c;
	}

	@Override
	public Operation addOperation(Operation o, String codeCpte, Long codeEmp) {
		//on charge les donn�es
		Compte c = consulterCompte(codeCpte);
		Employe emp = em.find(Employe.class, codeEmp);
		o.setCompte(c);
		o.setEmploye(emp);
		em.persist(o);
		return o;
	}

	@Override
	public Compte consulterCompte(String codeCpte) {
		Compte c = em.find(Compte.class, codeCpte);
		if(c==null) throw new RuntimeException("Compte "+codeCpte+" introuvable!");
		return c;
	}
	
	@Override
	public void versement(String codeCpte, double mt, Long codeEmp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void retrait(String codeCpte, double mt, Long codeEmp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void virement(String codeCpte1, String codeCpte2, double mt, Long codeEmp) {
		// TODO Auto-generated method stub
		
	}

	
	//PSQL une liste on passe par une requ�te
	//Pagination nom position
	@Override
	public List<Operation> consulterOperations(String codeCpte,int position,int nboperation) {
		// Query JPA de javax.persistence
		Query req = em.createQuery("select o from Operation o where o.compte.codeCompte=:x order by o.dateoperation desc");
		req.setParameter("x", codeCpte);
		//Pagination
		req.setFirstResult(position);
		req.setMaxResults(nboperation);
		return req.getResultList();
	}

	@Override
	public Client consulterClient(Long codeCli) {
		Client c = em.find(Client.class, codeCli);
		if(c==null) throw new RuntimeException("Client introuvable!");
		return c;
	}

	@Override
	public List<Client> consulterClient(String mc) {
		// Query JPA de javax.persistence
		Query req = em.createQuery("select c from Client c where c.nomClient like:x");
		req.setParameter("x", "%"+mc+"%");
		return req.getResultList();
	}

	@Override
	public List<Compte> getComptesByClient(Long codeCli) {
		// Query JPA de javax.persistence
		Query req = em.createQuery("select c from Compte c where c.Client.codeCli=:x");
		req.setParameter("x",codeCli);
		return req.getResultList();
	}

	@Override
	public List<Compte> getComptesByEmploye(Long codeEmp) {
		// Query JPA de javax.persistence
		Query req = em.createQuery("select c from Compte c where c.Employe.codeEmp=:x");
		req.setParameter("x",codeEmp);
		return req.getResultList();
	}

	@Override
	public List<Employe> getEmployes() {
		Query req = em.createQuery("select e from Empoye");
		return req.getResultList();
	}

	@Override
	public List<Groupe> getGroupes() {
		// Query JPA de javax.persistence
		Query req = em.createQuery("select g from Groupe");
		return req.getResultList();
	}

	@Override
	public List<Employe> getEmployesByGroupe(Long codeGroupe) {
		// Query JPA de javax.persistence
		Query req = em.createQuery("select e from Employe e where e.Groupe.codeGroupe=:x");
		req.setParameter("x",codeGroupe);
		return req.getResultList();
	}
	//Pagination
	@Override
	public long getNombreOperation(String numCpte) {
		Query req =em.createQuery("select count(o) from Operation o where o.compte.codeCompte=:x");
		req.setParameter("x", numCpte);
		//req.getFirstResult() pour un resultat
		return (Long) req.getResultList().get(0);
	}

	
}
