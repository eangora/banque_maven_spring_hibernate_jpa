package org.gestion.bp.entities;

import java.util.Date;

import javax.persistence.*;
//Heirte de compte donc on va utiliser discriminatorValue
//lors de la cr�ation de compteCourant la partie type de compte aura pour nom CC
@Entity
@DiscriminatorValue("CC")
public class CompteCourant extends Compte{
	private double decouvert;

	public CompteCourant() {
		super();
		// TODO Auto-generated constructor stub
	}

	//Nous avons fait appel au constructeur parent(c'est � dire Compte)
	public CompteCourant(String codeCompte, Date dateCreation, double solde, double decouvert) {
		super(codeCompte, dateCreation, solde);
		this.decouvert = decouvert;
	}

	public double getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(double decouvert) {
		this.decouvert = decouvert;
	}
	

}
