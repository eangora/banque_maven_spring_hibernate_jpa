package org.gestion.bp.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;
@Entity
@Table(name="CLIENTS")
public class Client implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	//Specifier le nom des colonnes pas obligatoire
	@Column(name="CODE_CLI")
	private Long codeClient;
	private String nomClient;
	private String adresseClient;
	//Un client peut avoir plusieurs comptes
	//on utilisera One to Many
	@OneToMany(mappedBy="client",fetch=FetchType.LAZY)
	private Collection<Compte>compte;
	public Client(String nomClient, String adresseClient) {
		super();
		this.nomClient = nomClient;
		this.adresseClient = adresseClient;
	}
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getCodeClient() {
		return codeClient;
	}
	public void setCodeClient(Long codeClient) {
		this.codeClient = codeClient;
	}
	public String getNomClient() {
		return nomClient;
	}
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}
	public String getAdresseClient() {
		return adresseClient;
	}
	public void setAdresseClient(String adresseClient) {
		this.adresseClient = adresseClient;
	}
	public Collection<Compte> getCompte() {
		return compte;
	}
	public void setCompte(Collection<Compte> compte) {
		this.compte = compte;
	}
	
	
}
