package org.gestion.bp.metier;

import java.util.Date;
import java.util.List;

import org.gestion.bp.dao.IBanqueDao;
import org.gestion.bp.entities.Client;
import org.gestion.bp.entities.Compte;
import org.gestion.bp.entities.Employe;
import org.gestion.bp.entities.Groupe;
import org.gestion.bp.entities.Operation;
import org.gestion.bp.entities.Retrait;
import org.gestion.bp.entities.Versement;
import org.springframework.transaction.annotation.Transactional;
//On dit � spring que toutes les m�thodes sont transactionnelles springtx
//On peut juste le sp�cifier sur chaque m�thode
@Transactional
public class BanqueMetierImpl implements IBanqueMetier {

	//Couplage avec la couche DAO
	private IBanqueDao dao;
	//On va cr�er 7 DAO dans Menu Source on g�n�re setter getter
	
	@Override
	public Client addClient(Client c) {
		// TODO Auto-generated method stub
		return dao.addClient(c);
	}

	public void setDao(IBanqueDao dao) {
		this.dao = dao;
	}

	@Override
	public Employe addEmploye(Employe e, Long codeSup) {
		// TODO Auto-generated method stub
		return dao.addEmploye(e, codeSup);
	}

	@Override
	public Groupe addGroupe(Groupe g) {
		// TODO Auto-generated method stub
		return dao.addGroupe(g);
	}

	@Override
	public void addEmployeToGroupe(Long codeEmp, Long codeGroupe) {
		// TODO Auto-generated method stub
		dao.addEmployeToGroupe(codeEmp, codeGroupe);
	}

	@Override
	public Compte addCompte(Compte c, Long codeCli, Long codeEmp) {
		// TODO Auto-generated method stub
		return dao.addCompte(c, codeCli, codeEmp);
	}

	@Override
	public void versement(String Cpte, double mt, Long codeEmp) {
		// TODO Auto-generated method stub
		dao.addOperation(new Versement(new Date(),mt), Cpte, codeEmp);
		//Mettre � jour le solde
		Compte c = dao.consulterCompte(Cpte);
		c.setSolde(c.getSolde()+mt);
	}

	@Override
	public void retrait(String Cpte, double mt, Long codeEmp) {
		// TODO Auto-generated method stub
		dao.addOperation(new Retrait(new Date(),mt), Cpte, codeEmp);
		//Mettre � jour le solde
		Compte c = dao.consulterCompte(Cpte);
		c.setSolde(c.getSolde()-mt);
		
	}

	@Override
	public void virement(String Cpte1, String Cpte2, double mt, Long codeEmp) {
		// TODO Auto-generated method stub
		retrait(Cpte1,mt,codeEmp);
		versement(Cpte2,mt,codeEmp);
	}

	@Override
	public Compte consulterCompte(String codeCpte) {
		// TODO Auto-generated method stub
		return dao.consulterCompte(codeCpte);
	}

	@Override
	public List<Operation> consulterOperations(String codeCpte,int position,int nboperation) {
		// TODO Auto-generated method stub
		return dao.consulterOperations(codeCpte, position, nboperation);
	}

	@Override
	public Client consulterClient(Long codeCli) {
		// TODO Auto-generated method stub
		return dao.consulterClient(codeCli);
	}

	@Override
	public List<Client> consulterClient(String mc) {
		// TODO Auto-generated method stub
		return dao.consulterClient(mc);
	}

	@Override
	public List<Compte> getComptesByClient(Long codeCli) {
		// TODO Auto-generated method stub
		return dao.getComptesByClient(codeCli);
	}

	@Override
	public List<Compte> getComptesByEmploye(Long codeEmp) {
		// TODO Auto-generated method stub
		return dao.getComptesByEmploye(codeEmp);
	}

	@Override
	public List<Employe> getEmployes() {
		// TODO Auto-generated method stub
		return dao.getEmployes();
	}

	@Override
	public List<Groupe> getGroupes() {
		// TODO Auto-generated method stub
		return dao.getGroupes();
	}

	@Override
	public List<Employe> getEmployesByGroupe(Long codeGroupe) {
		// TODO Auto-generated method stub
		return dao.getEmployesByGroupe(codeGroupe);
	}
	//Pagination
	@Override
	public long getNombreOperation(String numCpte) {
		
		return dao.getNombreOperation(numCpte);
	}
	

}
