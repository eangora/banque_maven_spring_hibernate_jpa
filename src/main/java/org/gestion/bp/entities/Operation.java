package org.gestion.bp.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
//Sans Discriminator il prendra les valeurs par d�faut
public class Operation implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long numeroOpertation;
	private Date dateoperation;
	private double montant;
	//Associations
	//Une operation appartient � un compte
	@ManyToOne
	//Cl� �trang�re
	@JoinColumn(name="CODE_CPTE")
	private Compte compte;
	//Une op�ration cr�er par un employe
	@ManyToOne
	//Cl� �trang�re
	@JoinColumn(name="CODE_EMP")
	private Employe employe;
	public Operation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Operation(Date dateoperation, double montant) {
		super();
		this.dateoperation = dateoperation;
		this.montant = montant;
	}
	public Long getNumeroOpertation() {
		return numeroOpertation;
	}
	public void setNumeroOpertation(Long numeroOpertation) {
		this.numeroOpertation = numeroOpertation;
	}
	public Date getDateoperation() {
		return dateoperation;
	}
	public void setDateoperation(Date dateoperation) {
		this.dateoperation = dateoperation;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	public Employe getEmploye() {
		return employe;
	}
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	

}
