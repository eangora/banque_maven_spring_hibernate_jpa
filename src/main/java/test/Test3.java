//package test;
//
//import java.util.List;
//
//import org.gestion.bp.entities.Compte;
//import org.gestion.bp.entities.Operation;
//import org.gestion.bp.metier.IBanqueMetier;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//public class Test3 {
//
//	public static void main(String[] args) {
//		ClassPathXmlApplicationContext context=
//				new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
//		//Couche Metier
//		IBanqueMetier metier=(IBanqueMetier) context.getBean("metier");
//		Compte c = metier.consulterCompte("CC1");
//		System.out.println("Solde:"+c.getSolde());
//		System.out.println("Date:"+c.getDateCreation());
//		System.out.println("Client:"+c.getClient().getNomClient());
//		System.out.println("Employe:"+c.getEmploye().getNomEmploye());
//		//Historique des opérations
//		List<Operation> ops= metier.consulterOperations("CC1");
//		for (Operation op:ops) {
//			System.out.println("*************************");
//			System.out.println(op.getNumeroOpertation());
//			System.out.println(op.getMontant());
//			System.out.println(op.getDateoperation());
//			//Obtenir le type d'opération
//			System.out.println(op.getClass().getSimpleName());
//		}
//	}
//
//}
