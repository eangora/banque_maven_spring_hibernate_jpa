package test;


import java.util.Date;

import org.gestion.bp.entities.Client;
import org.gestion.bp.entities.CompteCourant;
import org.gestion.bp.entities.CompteEpargne;
import org.gestion.bp.entities.Employe;
import org.gestion.bp.entities.Groupe;
import org.gestion.bp.metier.IBanqueMetier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test2 {

	public static void main(String[] args) {
	ClassPathXmlApplicationContext context=
				new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
		//Couche Metier
		IBanqueMetier metier=(IBanqueMetier) context.getBean("metier");
		//Ajout d'un client
		metier.addClient(new Client("Pablo","1234 rue de la porte Mantes"));
		metier.addClient(new Client("ANGORA","12 rue de l'arbre Paris"));
		metier.addEmploye(new Employe("Barth"), null);
		metier.addEmploye(new Employe("BOB"), 1L);
		//1L le L cest Long
		metier.addEmploye(new Employe("BABI"), 2L);
		metier.addGroupe(new Groupe("G1"));
		metier.addGroupe(new Groupe("G2"));
		metier.addGroupe(new Groupe("G3"));
		metier.addEmployeToGroupe(1L, 1L);
		metier.addEmployeToGroupe(2L, 1L);
		metier.addEmployeToGroupe(3L, 2L);
		metier.addCompte(new CompteCourant("CC1", new Date(), 9000, 8000), 1L, 2L);
		metier.addCompte(new CompteEpargne("CE1", new Date(), 4000, 5.5), 2L, 2L);
		metier.versement("CC1", 5000, 2L);
		metier.retrait("CC1", 6000, 2L);
		metier.virement("CC1", "CE1", 4000, 1L);
	}

}
