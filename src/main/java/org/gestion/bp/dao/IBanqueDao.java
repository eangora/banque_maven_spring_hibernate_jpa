package org.gestion.bp.dao;

import java.util.List;

import org.gestion.bp.entities.Client;
import org.gestion.bp.entities.Compte;
import org.gestion.bp.entities.Employe;
import org.gestion.bp.entities.Groupe;
import org.gestion.bp.entities.Operation;

public interface IBanqueDao {
	
	//On peut utiliser public void pour la m�thode mais on retourne l'objet client ici
	//Une m�thode qui permet de cr�er un client
	public Client addClient(Client c);
	//on cr�e un employ� et son sup�rieur hi�rarchique
	public Employe addEmploye(Employe e,Long codeSup);
	//On cr�e un groupe
	public Groupe addGroupe(Groupe g);
	//Avec la relation de plusieur � plusieurs
	public void addEmployeToGroupe(Long codeEmp,Long codeGroupe);
	//M�thode qui permet de cr�er un compte
	public Compte addCompte(Compte c,Long codeCli,Long codeEmp);
	//Cr�ation d'une op�ration
	public Operation addOperation(Operation o,String codeCpte,Long codeEmp);
	//Cr�er un versement
	public void versement(String codeCpte,double mt,Long codeEmp);
	//Cr�er un retrait
	public void retrait(String codeCpte,double mt,Long codeEmp);
	//Cr�er un virement
	public void virement(String codeCpte1,String codeCpte2,double mt,Long codeEmp);
	
	//On va consulter 
	
	//M�thode pour consulter un compte
	public Compte consulterCompte(String codeCpte);
	//Consulter les op�rations d'un compte
	public List<Operation> consulterOperations(String codeCpte,int position,int nboperation);
	public Client consulterClient(Long codeCli);
	//Consultation des listes de donn�es
	public List<Client> consulterClient(String mc);
	public List<Compte> getComptesByClient(Long codeCli);
	public List<Compte> getComptesByEmploye(Long codeEmp);
	public List<Employe> getEmployes();
	//Consulter la liste de groupe
	public List<Groupe> getGroupes();
	//Consulter les employ�s d'un groupe
	public List<Employe> getEmployesByGroupe(Long codeGroupe);
	//Methode pour la pagination
	public long getNombreOperation(String numCpte);

	
	
	
}
